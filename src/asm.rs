use crate::asm::ScanError::UnexpectedChar;
use crate::cpu::*;
use std::collections::HashMap;
use std::iter::Peekable;
use std::slice::Iter;
use std::str::Chars;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct SrcContext {
    pub line_no: u32,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Token {
    Comment(String),
    Name(String),
    Label(String),
    Asciz(String),
    Int(i32),
    Comma,
    ParenOpen,
    ParenClose,
    EOL,
    EOF,
}

pub fn scan(txt: &str) -> Result<Vec<(Token, SrcContext)>, ScanError> {
    let mut res: Vec<(Token, SrcContext)> = Vec::new();
    let mut ctx = SrcContext { line_no: 1u32 };
    let mut chars = txt.chars().peekable();

    loop {
        match chars.peek() {
            None => {
                res.push((Token::EOF, ctx));
                break;
            }
            Some('#') => res.push(scan_comment(&mut chars, ctx)?),
            Some('"') => res.push(scan_asciz_str(&mut chars, ctx)?),
            Some('\n') => {
                res.push((Token::EOL, ctx));
                ctx.line_no += 1;
                chars.next();
            }
            Some(i) if i.is_numeric() || (*i == '-') => {
                res.push(scan_int(&mut chars, ctx)?);
            }
            Some(',') => {
                res.push((Token::Comma, ctx));
                chars.next();
            }
            Some('(') => {
                res.push((Token::ParenOpen, ctx));
                chars.next();
            }
            Some(')') => {
                res.push((Token::ParenClose, ctx));
                chars.next();
            }
            Some(c) if c.is_whitespace() => {
                chars.next();
            }
            Some(&c) if c.is_ascii_alphabetic() || (c == '.') || (c == '_') => {
                res.push(scan_name_or_label(&mut chars, ctx)?);
            }
            Some(&other) => return Err(UnexpectedChar(other, ctx)),
        }
    }

    Ok(res)
}

#[derive(Debug)]
pub enum ScanError {
    UnexpectedChar(char, SrcContext),
    UnexpectedEOForEOL(SrcContext),
    InvalidCharFollowingEscape(char, SrcContext),
    InvalidNumericValue(char, SrcContext),
}

fn scan_comment(
    chars: &mut Peekable<Chars>,
    ctx: SrcContext,
) -> Result<(Token, SrcContext), ScanError> {
    let mut s = String::new();
    loop {
        match chars.peek() {
            None | Some('\n') => break,
            Some(&other) => s.push(other),
        }
        chars.next();
    }
    Ok((Token::Comment(s), ctx))
}

fn scan_asciz_str(
    chars: &mut Peekable<Chars>,
    ctx: SrcContext,
) -> Result<(Token, SrcContext), ScanError> {
    let mut s = String::new();
    if let Some(&'"') = chars.peek() {
        chars.next();
    }
    loop {
        match chars.next() {
            None | Some('\n') => {
                return Err(ScanError::UnexpectedEOForEOL(ctx));
            }
            Some('\\') => match chars.next() {
                Some('n') => s.push('\n'),
                Some('"') => s.push('"'),
                Some(other) => return Err(ScanError::InvalidCharFollowingEscape(other, ctx)),
                None => return Err(ScanError::UnexpectedEOForEOL(ctx)),
            },
            Some('"') => break,
            Some(other) => s.push(other),
        }
    }
    Ok((Token::Asciz(s), ctx))
}

fn scan_int(
    chars: &mut Peekable<Chars>,
    ctx: SrcContext,
) -> Result<(Token, SrcContext), ScanError> {
    let mut s = String::new();

    let sign = if let Some(&'-') = chars.peek() {
        chars.next();
        -1
    } else {
        1
    };

    loop {
        match chars.peek() {
            Some(&c) if c.is_numeric() => {
                s.push(c);
            }
            _other => {
                break;
            }
        }
        chars.next();
    }
    Ok((Token::Int(sign * s.parse::<i32>().unwrap()), ctx))
}

fn scan_name_or_label(
    chars: &mut Peekable<Chars>,
    ctx: SrcContext,
) -> Result<(Token, SrcContext), ScanError> {
    let mut s = String::new();
    let token = loop {
        match chars.peek() {
            Some(c) if c.is_alphanumeric() || (*c == '_') || (*c == '.') => s.push(*c),
            Some(':') => {
                chars.next();
                break Token::Label(s.clone());
            }
            _ => break Token::Name(s.clone()),
        }
        chars.next();
    };
    Ok((token, ctx))
}

#[derive(Copy, Clone, Debug)]
pub enum CodeGenError<'a> {
    SymbolNotFound(&'a str),
}

#[derive(Debug)]
pub struct RdRsImm {
    rd: RegisterIndex,
    rs: RegisterIndex,
    imm: i32,
}

impl RdRsImm {
    fn parse_from(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<Self, ParsingError> {
        let (rd, rs, imm) = match_reg_reg_imm(tokens_it)?;
        Ok(RdRsImm { rd, rs, imm })
    }
}

#[derive(Debug)]
pub struct Rs1Rs2Imm {
    rs1: RegisterIndex,
    rs2: RegisterIndex,
    imm: i32,
}

impl Rs1Rs2Imm {
    #[allow(dead_code)]
    fn parse_from(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<Self, ParsingError> {
        let (rs1, rs2, imm) = match_reg_reg_imm(tokens_it)?;
        Ok(Rs1Rs2Imm { rs1, rs2, imm })
    }
}

#[derive(Debug)]
pub struct RdRs1Rs2 {
    rd: RegisterIndex,
    rs1: RegisterIndex,
    rs2: RegisterIndex,
}

impl RdRs1Rs2 {
    fn parse_from(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<Self, ParsingError> {
        let (rd, rs1, rs2) = match_reg_reg_reg(tokens_it)?;
        Ok(RdRs1Rs2 { rd, rs1, rs2 })
    }
}

#[derive(Debug)]
pub struct RegSym {
    r: RegisterIndex,
    sym: String,
}

impl RegSym {
    fn parse_from(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<Self, ParsingError> {
        let r = match_reg_name(tokens_it)?;
        match_comma(tokens_it)?;
        let sym = match_symbol(tokens_it)?;
        Ok(RegSym { r, sym })
    }
}

#[derive(Debug)]
pub struct RegImm {
    r: RegisterIndex,
    imm: i32,
}

impl RegImm {
    fn parse_from(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<Self, ParsingError> {
        let r = match_reg_name(tokens_it)?;
        match_comma(tokens_it)?;
        let imm = match_imm_value(tokens_it)?;
        Ok(RegImm { r, imm })
    }
}

#[derive(Debug)]
pub struct Rs1Rs2Sym {
    rs1: RegisterIndex,
    rs2: RegisterIndex,
    sym: String,
}

impl Rs1Rs2Sym {
    fn parse_from(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<Self, ParsingError> {
        let (rs1, rs2, sym) = match_reg_reg_sym(tokens_it)?;
        Ok(Rs1Rs2Sym { rs1, rs2, sym })
    }
}

#[derive(Debug)]
pub struct RdRsImmAsOffset {
    rd: RegisterIndex,
    rs: RegisterIndex,
    offset: i32,
}

impl RdRsImmAsOffset {
    fn parse_from(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<Self, ParsingError> {
        let (rd, rs, offset) = match_reg_reg_imm_as_offset(tokens_it)?;
        Ok(Self { rd, rs, offset })
    }
}

#[derive(Debug)]
pub struct RdRs {
    rd: RegisterIndex,
    rs: RegisterIndex,
}

impl RdRs {
    fn parse_from(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<Self, ParsingError> {
        let rd = match_reg_name(tokens_it)?;
        match_comma(tokens_it)?;
        let rs = match_reg_name(tokens_it)?;
        Ok(Self { rd, rs })
    }
}

#[derive(Debug)]
pub enum ASTNNode {
    Label(String),
    AscizDeclaration(String, String),
    Add(RdRs1Rs2),
    Addi(RdRsImm),
    BeqImm(Rs1Rs2Imm),
    BeqSym(Rs1Rs2Sym),
    BgeSym(Rs1Rs2Sym),
    BltSym(Rs1Rs2Sym),
    BneImm(Rs1Rs2Imm),
    BneSym(Rs1Rs2Sym),
    Div(RdRs1Rs2),
    ECall,
    Ebreak,
    J(String),
    Jal(String),
    La(RegSym),
    Lb(RdRsImmAsOffset),
    Lw(RdRsImmAsOffset),
    Li(RegImm),
    Mul(RdRs1Rs2),
    Mv(RdRs),
    Rem(RdRs1Rs2),
    Ret,
    Sb(RdRsImmAsOffset),
    Sub(RdRs1Rs2),
    Sw(RdRsImmAsOffset),
    Xor(RdRs1Rs2),
}

impl ASTNNode {
    pub fn emit_instructions(&self, emitter: &mut CodeEmitter) -> Result<(), CodeGenError> {
        match self {
            ASTNNode::Label(_) => {}
            ASTNNode::AscizDeclaration(_label, ascii_str) => {
                ascii_str.bytes().for_each(|b| emitter.emit_u8(b));
                let remaining_bytes = 4 - ascii_str.len() % 4;
                for _ in 0..remaining_bytes {
                    emitter.emit_u8(0);
                }
            }
            ASTNNode::Add(ops) => emitter.emit(&add(ops.rd, ops.rs1, ops.rs2)),
            ASTNNode::Addi(ops) => emitter.emit(&addi(ops.rd, ops.rs, ops.imm)),
            ASTNNode::BeqImm(ops) => emitter.emit(&beq(ops.rs1, ops.rs2, ops.imm)),
            ASTNNode::BeqSym(ops) => {
                let Some(sym_addr) = emitter.symbol_addr(&ops.sym) else {return Err(CodeGenError::SymbolNotFound(&ops.sym));};
                let imm = ((sym_addr as i64) - (emitter.current_addr() as i64)) as i32;
                emitter.emit(&beq(ops.rs1, ops.rs2, imm));
            }
            ASTNNode::BgeSym(ops) => {
                let Some(sym_addr) = emitter.symbol_addr(&ops.sym) else {return Err(CodeGenError::SymbolNotFound(&ops.sym));};
                let imm = ((sym_addr as i64) - (emitter.current_addr() as i64)) as i32;
                emitter.emit(&bge(ops.rs1, ops.rs2, imm));
            }
            ASTNNode::BltSym(ops) => {
                let Some(sym_addr) = emitter.symbol_addr(&ops.sym) else {return Err(CodeGenError::SymbolNotFound(&ops.sym));};
                let imm = ((sym_addr as i64) - (emitter.current_addr() as i64)) as i32;
                emitter.emit(&blt(ops.rs1, ops.rs2, imm));
            }
            ASTNNode::BneImm(ops) => emitter.emit(&bne(ops.rs1, ops.rs2, ops.imm)),
            ASTNNode::BneSym(ops) => {
                let Some(sym_addr) = emitter.symbol_addr(&ops.sym) else {return Err(CodeGenError::SymbolNotFound(&ops.sym));};
                let imm = ((sym_addr as i64) - (emitter.current_addr() as i64)) as i32;
                emitter.emit(&bne(ops.rs1, ops.rs2, imm));
            }
            ASTNNode::Div(ops) => emitter.emit(&div(ops.rd, ops.rs1, ops.rs2)),
            ASTNNode::ECall => emitter.emit(&ecall()),
            ASTNNode::Ebreak => emitter.emit(&ebreak()),
            ASTNNode::J(sym) | ASTNNode::Jal(sym) => {
                let Some(sym_addr) = emitter.symbol_addr(sym) else {return Err(CodeGenError::SymbolNotFound(sym));};
                let imm = ((sym_addr as i64) - (emitter.current_addr() as i64)) as i32;
                let link_reg = if let ASTNNode::J(_) = self { 0 } else { 1 };
                emitter.emit(&jal(imm, link_reg)); // x1 is ra
            }
            ASTNNode::La(ops) => {
                let Some(sym_addr) = emitter.symbol_addr(&ops.sym) else {return Err(CodeGenError::SymbolNotFound(&ops.sym));};
                emitter.emit(&addi(ops.r, 0, sym_addr as i32));
            }
            ASTNNode::Lb(ops) => emitter.emit(&lb(ops.rd, ops.rs, ops.offset)),
            ASTNNode::Lw(ops) => emitter.emit(&lw(ops.rd, ops.rs, ops.offset)),
            ASTNNode::Li(ops) => emitter.emit(&addi(ops.r, 0, ops.imm)),
            ASTNNode::Mul(ops) => emitter.emit(&mul(ops.rd, ops.rs1, ops.rs2)),
            ASTNNode::Mv(ops) => emitter.emit(&addi(ops.rd, ops.rs, 0)),
            ASTNNode::Rem(ops) => emitter.emit(&rem(ops.rd, ops.rs1, ops.rs2)),
            ASTNNode::Ret => emitter.emit(&jalr(0, 1, 0)),
            ASTNNode::Sb(ops) => emitter.emit(&sb(ops.rs, ops.rd, ops.offset)),
            ASTNNode::Sub(ops) => emitter.emit(&sub(ops.rd, ops.rs1, ops.rs2)),
            ASTNNode::Sw(ops) => emitter.emit(&sw(ops.rs, ops.rd, ops.offset)),
            ASTNNode::Xor(ops) => emitter.emit(&xor(ops.rd, ops.rs1, ops.rs2)),
        }

        Ok(())
    }

    pub fn instr_size(&self) -> u32 {
        match self {
            ASTNNode::Label(_) => 0,
            ASTNNode::AscizDeclaration(_label, s) => (s.len() + 4 - s.len() % 4) as u32,
            _other => 4,
        }
    }
}

#[derive(Debug)]
pub struct CodeEmitter {
    pub buffer: Vec<u8>,
    sym_addrs: HashMap<String, u32>,
}

impl CodeEmitter {
    pub fn new() -> CodeEmitter {
        let mut res = CodeEmitter {
            buffer: Vec::new(),
            sym_addrs: HashMap::new(),
        };
        res.emit(&nop());
        res
    }

    fn emit(&mut self, instr: &Instruction) {
        let encoded = instr.encode();
        self.emit_u32(encoded);
    }

    fn emit_u8(&mut self, v: u8) {
        self.buffer.push(v);
    }

    fn emit_u32(&mut self, v: u32) {
        for i in 0..4 {
            self.buffer.push(((v >> (i * 8)) & 0xFF) as u8);
        }
    }

    fn register_symbol(&mut self, sym: String, addr: u32) {
        self.sym_addrs.insert(sym, addr);
    }

    fn symbol_addr(&self, sym: &str) -> Option<u32> {
        self.sym_addrs.get(sym).copied()
    }

    fn current_addr(&self) -> u32 {
        self.buffer.len() as u32
    }

    fn set_entry_point_jump(&mut self, addr: u32) {
        let instr = jal(addr as i32, 0).encode();
        self.buffer[0] = (instr & 0xFF) as u8;
        self.buffer[1] = ((instr >> 8) & 0xFF) as u8;
        self.buffer[2] = ((instr >> 16) & 0xFF) as u8;
        self.buffer[3] = ((instr >> 24) & 0xFF) as u8;
    }
}

impl Default for CodeEmitter {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Debug)]
pub enum ParsingError {
    UnsupportedMnemonic(String, Option<SrcContext>),
    ExpectingEndOfLineOrEndOfFile(Option<Token>, Option<SrcContext>),
    ExpectingComma(Option<SrcContext>),
    ExpectingParenOpen(Option<SrcContext>),
    ExpectingParenClose(Option<SrcContext>),
    ExpectingRegisterName(Option<SrcContext>),
    InvalidRegisterName(String),
    ExpectingImmediateValue(Option<SrcContext>),
    UnableToParseInstruction(Option<SrcContext>),
    ExpectingSymbol(Option<SrcContext>),
    UnableToParseAsciiString(Option<SrcContext>),
}

fn match_eol_or_eof(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<(), ParsingError> {
    match tokens_it.next() {
        Some((Token::EOL, _)) => Ok(()),
        Some((Token::EOF, _)) => Ok(()),
        Some((token, ctx)) => Err(ParsingError::ExpectingEndOfLineOrEndOfFile(
            Some(token.clone()),
            Some(*ctx),
        )),
        _ => Err(ParsingError::ExpectingEndOfLineOrEndOfFile(None, None)),
    }
}

fn match_comma(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<(), ParsingError> {
    match tokens_it.next() {
        Some((Token::Comma, _)) => Ok(()),
        Some((_, ctx)) => Err(ParsingError::ExpectingComma(Some(*ctx))),
        _ => Err(ParsingError::ExpectingComma(None)),
    }
}

fn match_paren_open(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<(), ParsingError> {
    match tokens_it.next() {
        Some((Token::ParenOpen, _)) => Ok(()),
        Some((_, ctx)) => Err(ParsingError::ExpectingParenOpen(Some(*ctx))),
        _ => Err(ParsingError::ExpectingParenOpen(None)),
    }
}

fn match_paren_closed(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<(), ParsingError> {
    match tokens_it.next() {
        Some((Token::ParenClose, _)) => Ok(()),
        Some((_, ctx)) => Err(ParsingError::ExpectingParenClose(Some(*ctx))),
        _ => Err(ParsingError::ExpectingParenClose(None)),
    }
}

fn match_reg_name(
    tokens_it: &mut Iter<(Token, SrcContext)>,
) -> Result<RegisterIndex, ParsingError> {
    match tokens_it.next() {
        Some((Token::Name(name), _)) => {
            if let Some(reg_index) = mnemo2regi(name) {
                Ok(reg_index)
            } else {
                Err(ParsingError::InvalidRegisterName(name.clone()))
            }
        }
        Some((_, ctx)) => Err(ParsingError::ExpectingRegisterName(Some(*ctx))),
        _ => Err(ParsingError::ExpectingRegisterName(None)),
    }
}

fn match_imm_value(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<i32, ParsingError> {
    match tokens_it.next() {
        Some((Token::Int(i), _)) => Ok(*i),
        Some((_, ctx)) => Err(ParsingError::ExpectingImmediateValue(Some(*ctx))),
        _ => Err(ParsingError::ExpectingImmediateValue(None)),
    }
}

fn match_symbol(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<String, ParsingError> {
    match tokens_it.next() {
        Some((Token::Name(name), _)) => Ok(name.clone()),
        Some((_, ctx)) => Err(ParsingError::ExpectingSymbol(Some(*ctx))),
        _ => Err(ParsingError::ExpectingSymbol(None)),
    }
}

fn match_reg_reg_sym(
    tokens_it: &mut Iter<(Token, SrcContext)>,
) -> Result<(RegisterIndex, RegisterIndex, String), ParsingError> {
    let r1 = match_reg_name(tokens_it)?;
    match_comma(tokens_it)?;
    let r2 = match_reg_name(tokens_it)?;
    match_comma(tokens_it)?;
    let sym = match_symbol(tokens_it)?;

    Ok((r1, r2, sym))
}

fn match_reg_reg_imm(
    tokens_it: &mut Iter<(Token, SrcContext)>,
) -> Result<(RegisterIndex, RegisterIndex, i32), ParsingError> {
    let r1 = match_reg_name(tokens_it)?;
    match_comma(tokens_it)?;
    let r2 = match_reg_name(tokens_it)?;
    match_comma(tokens_it)?;
    let imm = match_imm_value(tokens_it)?;

    Ok((r1, r2, imm))
}

fn match_reg_reg_imm_as_offset(
    tokens_it: &mut Iter<(Token, SrcContext)>,
) -> Result<(RegisterIndex, RegisterIndex, i32), ParsingError> {
    let r1 = match_reg_name(tokens_it)?;
    match_comma(tokens_it)?;
    let imm = match_imm_value(tokens_it)?;
    match_paren_open(tokens_it)?;
    let r2 = match_reg_name(tokens_it)?;
    match_paren_closed(tokens_it)?;

    Ok((r1, r2, imm))
}

fn match_reg_reg_reg(
    tokens_it: &mut Iter<(Token, SrcContext)>,
) -> Result<(RegisterIndex, RegisterIndex, RegisterIndex), ParsingError> {
    let r1 = match_reg_name(tokens_it)?;
    match_comma(tokens_it)?;
    let r2 = match_reg_name(tokens_it)?;
    match_comma(tokens_it)?;
    let r3 = match_reg_name(tokens_it)?;

    Ok((r1, r2, r3))
}

fn parse_beq(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<ASTNNode, ParsingError> {
    let node = if let Ok((rs1, rs2, imm)) = match_reg_reg_imm(&mut tokens_it.clone()) {
        ASTNNode::BeqImm(Rs1Rs2Imm { rs1, rs2, imm })
    } else if let Ok((rs1, rs2, sym)) = match_reg_reg_sym(&mut tokens_it.clone()) {
        ASTNNode::BeqSym(Rs1Rs2Sym { rs1, rs2, sym })
    } else {
        match tokens_it.peekable().peek() {
            Some((_, ctx)) => return Err(ParsingError::UnableToParseInstruction(Some(*ctx))),
            None => return Err(ParsingError::UnableToParseInstruction(None)),
        }
    };
    for _ in 0..5 {
        tokens_it.next();
    }
    Ok(node)
}

fn parse_bne(tokens_it: &mut Iter<(Token, SrcContext)>) -> Result<ASTNNode, ParsingError> {
    let node = if let Ok((rs1, rs2, imm)) = match_reg_reg_imm(&mut tokens_it.clone()) {
        ASTNNode::BneImm(Rs1Rs2Imm { rs1, rs2, imm })
    } else if let Ok((rs1, rs2, sym)) = match_reg_reg_sym(&mut tokens_it.clone()) {
        ASTNNode::BneSym(Rs1Rs2Sym { rs1, rs2, sym })
    } else {
        match tokens_it.peekable().peek() {
            Some((_, ctx)) => return Err(ParsingError::UnableToParseInstruction(Some(*ctx))),
            None => return Err(ParsingError::UnableToParseInstruction(None)),
        }
    };
    for _ in 0..5 {
        tokens_it.next();
    }
    Ok(node)
}

fn parse_asciz(
    label: String,
    out: &mut Vec<ASTNNode>,
    tokens_it: &mut Iter<(Token, SrcContext)>,
) -> Result<(), ParsingError> {
    tokens_it.next();
    match tokens_it.next() {
        Some((Token::Asciz(ascii_str), _)) => {
            out.push(ASTNNode::AscizDeclaration(label, ascii_str.clone()))
        }
        Some((_, ctx)) => return Err(ParsingError::UnableToParseAsciiString(Some(*ctx))),
        _ => return Err(ParsingError::UnableToParseAsciiString(None)),
    }
    Ok(())
}

pub fn parse(tokens: Vec<(Token, SrcContext)>) -> Result<Vec<ASTNNode>, ParsingError> {
    let mut res: Vec<ASTNNode> = Vec::new();
    let tokens_without_comments = tokens
        .iter()
        .filter(|(token, _)| !matches!(token, Token::Comment(_)))
        .cloned()
        .collect::<Vec<(Token, SrcContext)>>();
    let mut tokens_it: Iter<(Token, SrcContext)> = tokens_without_comments.iter();
    let mut maybe_token_with_ctx: Option<&(Token, SrcContext)>;

    loop {
        maybe_token_with_ctx = tokens_it.next();

        let Some((current_token, ctx)) = maybe_token_with_ctx else {break;};

        match current_token {
            Token::Comment(_) => (),

            Token::Label(label) => match tokens_it.clone().next() {
                Some((Token::Name(s), _)) if s == ".asciz" => {
                    parse_asciz(label.clone(), &mut res, &mut tokens_it)?
                }
                _ => {
                    res.push(ASTNNode::Label(label.clone()));
                }
            },

            Token::Name(mnemonic) => {
                let node = match mnemonic.to_lowercase().as_str() {
                    "add" => ASTNNode::Add(RdRs1Rs2::parse_from(&mut tokens_it)?),
                    "addi" => ASTNNode::Addi(RdRsImm::parse_from(&mut tokens_it)?),
                    "blt" => ASTNNode::BltSym(Rs1Rs2Sym::parse_from(&mut tokens_it)?),
                    "bne" => parse_bne(&mut tokens_it)?,
                    "beq" => parse_beq(&mut tokens_it)?,
                    "bge" => ASTNNode::BgeSym(Rs1Rs2Sym::parse_from(&mut tokens_it)?),
                    "ebreak" => ASTNNode::Ebreak,
                    "ecall" => ASTNNode::ECall,
                    "div" => ASTNNode::Div(RdRs1Rs2::parse_from(&mut tokens_it)?),
                    "j" => ASTNNode::J(match_symbol(&mut tokens_it)?),
                    "jal" => ASTNNode::Jal(match_symbol(&mut tokens_it)?),
                    "la" => ASTNNode::La(RegSym::parse_from(&mut tokens_it)?),
                    "lb" => ASTNNode::Lb(RdRsImmAsOffset::parse_from(&mut tokens_it)?),
                    "li" => ASTNNode::Li(RegImm::parse_from(&mut tokens_it)?),
                    "lw" => ASTNNode::Lw(RdRsImmAsOffset::parse_from(&mut tokens_it)?),
                    "mul" => ASTNNode::Mul(RdRs1Rs2::parse_from(&mut tokens_it)?),
                    "mv" => ASTNNode::Mv(RdRs::parse_from(&mut tokens_it)?),
                    "rem" => ASTNNode::Rem(RdRs1Rs2::parse_from(&mut tokens_it)?),
                    "ret" => ASTNNode::Ret,
                    "sb" => ASTNNode::Sb(RdRsImmAsOffset::parse_from(&mut tokens_it)?),
                    "sub" => ASTNNode::Sub(RdRs1Rs2::parse_from(&mut tokens_it)?),
                    "sw" => ASTNNode::Sw(RdRsImmAsOffset::parse_from(&mut tokens_it)?),
                    "xor" => ASTNNode::Xor(RdRs1Rs2::parse_from(&mut tokens_it)?),
                    other => Err(ParsingError::UnsupportedMnemonic(
                        other.to_string(),
                        Some(*ctx),
                    ))?,
                };
                match_eol_or_eof(&mut tokens_it)?;
                res.push(node);
            }

            _ => (),
        }
    }
    Ok(res)
}

pub fn mnemo2regi(reg: &str) -> Option<RegisterIndex> {
    let register_name_2_index: HashMap<&str, u32> = HashMap::from([
        ("zero", 0),
        ("ra", 1),
        ("sp", 2),
        ("gp", 3),
        ("tp", 4),
        ("t0", 5),
        ("t1", 6),
        ("t2", 7),
        ("s0", 8),
        ("fp", 8),
        ("s1", 9),
        ("a0", 10),
        ("a1", 11),
        ("a2", 12),
        ("a3", 13),
        ("a4", 14),
        ("a5", 15),
        ("a6", 16),
        ("a7", 17),
        ("s2", 18),
        ("s3", 19),
        ("s4", 20),
        ("s5", 21),
        ("s6", 22),
        ("s7", 23),
        ("s8", 24),
        ("s9", 25),
        ("s10", 26),
        ("s11", 27),
        ("t3", 28),
        ("t4", 29),
        ("t5", 30),
        ("t6", 31),
    ]);
    let reg_name = reg.to_lowercase();
    if register_name_2_index.contains_key(reg_name.as_str()) {
        Some(*register_name_2_index.get(reg_name.as_str()).unwrap() as RegisterIndex)
    } else {
        None
    }
}

pub fn gen_code<'a>(
    ast: &'a Vec<ASTNNode>,
    emitter: &'a mut CodeEmitter,
) -> Result<(), CodeGenError<'a>> {
    let mut next_addr = 4;

    for node in ast {
        if let ASTNNode::AscizDeclaration(label, _) = node {
            emitter.register_symbol(label.clone(), next_addr);
            node.emit_instructions(emitter)?;
            next_addr += node.instr_size();
        }
    }
    emitter.set_entry_point_jump(next_addr);

    for node in ast {
        match node {
            ASTNNode::Label(label) => emitter.register_symbol(label.clone(), next_addr),
            ASTNNode::AscizDeclaration(_, _) => {}
            _ => {
                next_addr += node.instr_size();
            }
        }
    }

    if let Some(addr) = emitter.symbol_addr("start") {
        emitter.set_entry_point_jump(addr);
    }

    for node in ast {
        match node {
            ASTNNode::AscizDeclaration(_, _) => {}
            ASTNNode::Label(_) => {}
            _ => node.emit_instructions(emitter)?,
        }
    }

    emitter.emit(&ebreak());

    Ok(())
}
