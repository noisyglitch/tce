use std::env;
use std::fs;
use std::io::Write;
use std::process::exit;
use toycpu::asm::gen_code;
use toycpu::asm::{parse, scan, CodeEmitter};

struct AsArgsPartial {
    maybe_input_file_path: Option<String>,
    maybe_output_bin_path: Option<String>,
    display_tokens: bool,
    display_ast: bool,
}

impl AsArgsPartial {
    fn new() -> AsArgsPartial {
        AsArgsPartial {
            maybe_input_file_path: None,
            maybe_output_bin_path: None,
            display_tokens: false,
            display_ast: false,
        }
    }
}

struct AsArgs {
    input_file_path: String,
    output_bin_path: String,
    display_tokens: bool,
    display_ast: bool,
}

impl AsArgs {
    fn usage() {
        println!("USAGE:");
        println!("\tas [OPTIONS]");
        println!("OPTIONS:");
        println!(
            "\t{option:<20}path to input asm source file",
            option = "-i <filename>"
        );
        println!(
            "\t{option:<20}path to output binary",
            option = "-o <filename>"
        );
        println!("\t{option:<20}display scanned tokens", option = "--tokens");
        println!("\t{option:<20}display AST", option = "--ast");
    }

    fn build_from(args: Vec<String>) -> Result<AsArgs, ()> {
        let mut i = 1;
        let mut partial = AsArgsPartial::new();

        loop {
            if i >= args.len() {
                break;
            }

            match args[i].trim() {
                "-i" => {
                    i += 1;
                    let Some( input_file_path) = args.get(i) else {
                        return Err(());
                    };
                    partial.maybe_input_file_path = Some(input_file_path.clone())
                }
                "-o" => {
                    i += 1;
                    let Some(output_bin_path) = args.get(i) else {
                        return Err(())
                    };
                    partial.maybe_output_bin_path = Some(output_bin_path.clone());
                }
                "--tokens" => partial.display_tokens = true,
                "--ast" => partial.display_ast = true,
                _other => return Err(()),
            }

            i += 1;
        }

        Ok(AsArgs {
            input_file_path: partial.maybe_input_file_path.ok_or(())?,
            output_bin_path: partial.maybe_output_bin_path.ok_or(())?,
            display_tokens: partial.display_tokens,
            display_ast: partial.display_ast,
        })
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let Ok(parsed_args) = AsArgs::build_from(args) else {
        AsArgs::usage();
        exit(-1);
    };

    let txt = fs::read_to_string(parsed_args.input_file_path).expect("Unable to read file");

    let tokens = match scan(&txt) {
        Ok(tokens) => tokens,
        Err(scan_error) => {
            println!("Tokenization error: {:?}", scan_error);
            exit(-1);
        }
    };

    if parsed_args.display_tokens {
        for ref token in &tokens {
            println!("{:?}", token);
        }
        println!("----------------------------------\n\n");
    }

    let ast = match parse(tokens) {
        Ok(parsed_ast) => parsed_ast,
        Err(parsing_error) => {
            println!("Parsing error: {:?}", parsing_error);
            exit(-1);
        }
    };

    if parsed_args.display_ast {
        for ref node in &ast {
            println!("{:?}", node);
        }
        println!("----------------------------------\n\n");
    }

    let mut emitter = CodeEmitter::new();
    if let Err(gen_err) = gen_code(&ast, &mut emitter) {
        println!("{:?}", gen_err);
        exit(-1);
    }

    fs::File::create(parsed_args.output_bin_path)
        .unwrap()
        .write_all(emitter.buffer.as_slice())
        .expect("Unable to write file");
}
