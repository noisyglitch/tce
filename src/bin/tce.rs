use std::process::exit;
use std::{env, fs};
use toycpu::cpu;
use toycpu::cpu::Instruction;

struct VSimArgsPartial {
    maybe_bin_path: Option<String>,
    display_instr: bool,
    display_regs: bool,
}

impl VSimArgsPartial {
    fn new() -> VSimArgsPartial {
        VSimArgsPartial {
            maybe_bin_path: None,
            display_instr: false,
            display_regs: false,
        }
    }
}

struct VSimArgs {
    bin_path: String,
    display_instr: bool,
    display_regs: bool,
}

impl VSimArgs {
    fn usage() {
        println!("USAGE:");
        println!("\tvsim [OPTIONS]");
        println!("OPTIONS");
        println!("\t{option:<20}path to binary", option = "-i <filename>");
        println!(
            "\t{option:<20}print each executed instruction",
            option = "--show-instr"
        );
        println!(
            "\t{option:<20}print registers after each executed instruction",
            option = "--show-regs"
        );
    }

    fn build_from(args: Vec<String>) -> Result<VSimArgs, ()> {
        let mut i = 1;
        let mut partial = VSimArgsPartial::new();

        loop {
            if i >= args.len() {
                break;
            }

            match args[i].trim() {
                "-i" => {
                    i += 1;
                    let Some(input_bin_path) = args.get(i) else {
                        return Err(());
                    };
                    partial.maybe_bin_path = Some(input_bin_path.clone());
                }
                "--show-instr" => partial.display_instr = true,
                "--show-regs" => partial.display_regs = true,
                _other => return Err(()),
            }

            i += 1;
        }

        Ok(VSimArgs {
            bin_path: partial.maybe_bin_path.ok_or(())?,
            display_instr: partial.display_instr,
            display_regs: partial.display_regs,
        })
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let Ok(parsed_args) = VSimArgs::build_from(args) else {
        VSimArgs::usage();
        exit(-1);
    };

    let prog = fs::read(parsed_args.bin_path).expect("Unable to read executable");
    let mut cpu = cpu::QDCPU::new();

    cpu.load(prog);
    if parsed_args.display_regs {
        cpu.print_registers();
    }
    loop {
        match cpu.step() {
            Ok(trace) => {
                if parsed_args.display_instr {
                    println!("{:?}", *trace.decoded_instr);
                }
                if parsed_args.display_regs {
                    cpu.print_registers();
                }
                if let Instruction::EBREAK(_) = *trace.decoded_instr {
                    break;
                }
            }
            Err(exec_error) => {
                println!("{:?}", exec_error);
                exit(1);
            }
        }
    }
}
