use crate::asm::mnemo2regi;
use crate::cpu::DecodingError::{UnsupportedOpcode, UnsupportedSubVariant, UnsupportedVariant};
use crate::cpu::ExecError::{DecodingIssue, UnreachableInstruction};
use crate::utils::{bits_value, sign_expanded_value, to2c};

pub type RegisterIndex = usize;

pub type ProgCounter = usize;

#[derive(Debug)]
pub enum ExecError {
    UnreachableInstruction(u32),
    DecodingIssue(DecodingError),
}

#[derive(Debug)]
pub enum DecodingError {
    UnsupportedOpcode(u32),
    UnsupportedVariant(u32, u32),
    UnsupportedSubVariant(u32, u32, u32),
}

pub struct ExecTrace {
    pub decoded_instr: Box<Instruction>,
}

#[derive(Debug)]
pub struct QDCPU {
    registers: [u32; 32],
    pub pc: ProgCounter,
    pub mem: [u8; 1024],
    pub prog: Vec<u8>,
}

impl QDCPU {
    pub fn new() -> QDCPU {
        QDCPU {
            registers: [0u32; 32],
            pc: 0,
            mem: [0u8; 1024],
            prog: Vec::new(),
        }
    }

    pub fn load(&mut self, prog: Vec<u8>) {
        self.prog = prog;
        self.reset();
    }

    pub fn reset(&mut self) {
        self.pc = 0;
        self.mem = [0u8; 1024];
        self.set_reg(2, 1023); // sp
        for i in 0..self.prog.len() {
            self.mem[i] = self.prog[i];
        }
    }

    pub fn step(&mut self) -> Result<ExecTrace, ExecError> {
        if self.pc + 3 >= self.mem.len() {
            return Err(UnreachableInstruction(self.pc as u32));
        }

        let instr = (self.mem[self.pc] as u32)
            + ((self.mem[self.pc + 1] as u32) << 8)
            + ((self.mem[self.pc + 2] as u32) << 16)
            + ((self.mem[self.pc + 3] as u32) << 24);

        let decoded_instr = match Instruction::decode(instr) {
            Ok(instr) => instr,
            Err(decoding_err) => return Err(DecodingIssue(decoding_err)),
        };

        decoded_instr.exec(self);

        Ok(ExecTrace { decoded_instr })
    }

    fn set_reg(&mut self, i: RegisterIndex, v: u32) {
        if i > 0 {
            self.registers[i] = v;
        }
    }

    fn get_reg(&self, i: RegisterIndex) -> u32 {
        if i == 0 {
            0
        } else {
            self.registers[i]
        }
    }

    pub fn regs(&self) -> &[u32; 32] {
        &self.registers
    }

    pub fn print_registers(&self) {
        let registers_order = [
            "ra", "sp", "gp", "tp", "fp", "t0", "t1", "t2", "t3", "t4", "t5", "t6", "a0", "a1",
            "a2", "a3", "a4", "a5", "a6", "a7", "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8",
            "s9", "s10", "s11",
        ];
        print!("[");
        print!("0: 0");
        for regname in registers_order {
            let i = mnemo2regi(regname).unwrap();
            print!(", {}: {}", regname, self.registers[i]);
        }
        println!("]");
    }
}

impl Default for QDCPU {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug)]
pub struct IType {
    pub rd: RegisterIndex,
    pub rs: RegisterIndex,
    pub imm: u32,
    pub funct3: u32,
}

impl IType {
    pub fn encode(&self, opcode: u32) -> u32 {
        opcode
            | (self.rd as u32) << 7
            | self.funct3 << 12
            | (self.rs as u32) << 15
            | bits_value(self.imm as u32, 0, 12) << 20
    }

    fn decode(instr: u32) -> IType {
        Self {
            rd: bits_value(instr, 7, 12) as RegisterIndex,
            rs: bits_value(instr, 15, 20) as RegisterIndex,
            imm: sign_expanded_value(instr, 20, 32),
            funct3: bits_value(instr, 12, 15),
        }
    }
}

#[derive(Debug)]
pub struct RType {
    pub rd: RegisterIndex,
    pub rs1: RegisterIndex,
    pub rs2: RegisterIndex,
    pub funct3: u32,
    pub funct7: u32,
}

impl RType {
    pub fn encode(&self, opcode: u32) -> u32 {
        opcode
            | (self.rd as u32) << 7
            | (self.rs1 as u32) << 15
            | (self.rs2 as u32) << 20
            | (self.funct3 as u32) << 12
            | (self.funct7 as u32) << 25
    }

    fn decode(instr: u32) -> RType {
        Self {
            rd: bits_value(instr, 7, 12) as RegisterIndex,
            rs1: bits_value(instr, 15, 20) as RegisterIndex,
            rs2: bits_value(instr, 20, 25) as RegisterIndex,
            funct3: bits_value(instr, 12, 15),
            funct7: bits_value(instr, 25, 32),
        }
    }
}

pub struct UType {
    pub rd: RegisterIndex,
    pub imm: u32,
}

impl UType {
    pub fn encode(&self, opcode: u32) -> u32 {
        opcode | (self.rd as u32) << 7 | self.imm & 0xFFFFF000
    }

    pub fn decode(instr: u32) -> UType {
        Self {
            rd: bits_value(instr, 7, 12) as RegisterIndex,
            imm: instr & 0xFFFFF000,
        }
    }
}

#[derive(Debug)]
pub struct SType {
    pub imm: u32,
    pub funct3: u32,
    pub rs1: RegisterIndex,
    pub rs2: RegisterIndex,
}

impl SType {
    pub fn encode(&self, opcode: u32) -> u32 {
        opcode
            | (self.imm & 0x0F) << 7
            | self.funct3 << 12
            | (self.rs1 as u32) << 15
            | (self.rs2 as u32) << 20
            | (self.imm >> 5) << 5
    }

    pub fn decode(instr: u32) -> SType {
        Self {
            imm: sign_expanded_value(
                bits_value(instr, 7, 12) | (bits_value(instr, 25, 32) << 5),
                0,
                13,
            ),
            rs1: bits_value(instr, 15, 20) as RegisterIndex,
            rs2: bits_value(instr, 20, 25) as RegisterIndex,
            funct3: bits_value(instr, 12, 15),
        }
    }
}

#[derive(Debug)]
pub struct JType {
    pub imm: u32,
    pub rd: RegisterIndex,
}

impl JType {
    pub fn encode(&self, opcode: u32) -> u32 {
        opcode
            | (self.rd as u32) << 7
            | bits_value(self.imm, 12, 20) << 12
            | ((self.imm >> 11) & 1) << 20
            | bits_value(self.imm, 1, 11) << 21
            | ((self.imm >> 20) & 1) << 31
    }

    pub fn decode(instr: u32) -> JType {
        Self {
            imm: sign_expanded_value(
                (bits_value(instr, 21, 31) << 1)
                    | (((instr >> 20) & 1) << 11)
                    | (bits_value(instr, 12, 20) << 12)
                    | (((instr >> 31) & 1) << 20),
                0,
                21,
            ),
            rd: bits_value(instr, 7, 12) as RegisterIndex,
        }
    }
}

#[derive(Debug)]
pub struct BType {
    pub imm: u32,
    pub rs1: RegisterIndex,
    pub rs2: RegisterIndex,
    pub funct3: u32,
}

impl BType {
    pub fn encode(&self, opcode: u32) -> u32 {
        opcode
            | ((self.imm & (1 << 11)) >> 11) << 7
            | bits_value(self.imm, 1, 5) << 8
            | self.funct3 << 12
            | (self.rs1 as u32) << 15
            | (self.rs2 as u32) << 20
            | bits_value(self.imm, 5, 11) << 25
            | ((self.imm & (1 << 12)) >> 12) << 31
    }

    pub fn decode(instr: u32) -> BType {
        Self {
            imm: sign_expanded_value(
                (bits_value(instr, 8, 12) << 1)
                    | (bits_value(instr, 25, 31) << 5)
                    | (((instr & (1 << 7)) >> 7) << 11)
                    | (((instr & (1 << 31)) >> 31) << 12),
                0,
                13,
            ),
            rs1: bits_value(instr, 15, 20) as RegisterIndex,
            rs2: bits_value(instr, 20, 25) as RegisterIndex,
            funct3: bits_value(instr, 12, 15),
        }
    }
}

#[derive(Debug)]
pub enum Instruction {
    Add(RType),
    AddI(IType),
    BEQ(BType),
    BGE(BType),
    BLT(BType),
    BNE(BType),
    DIV(RType),
    EBREAK(IType),
    ECALL(IType),
    JAL(JType),
    JALR(IType),
    LB(IType),
    LW(IType),
    MUL(RType),
    REM(RType),
    SB(SType),
    SltI(IType),
    Sub(RType),
    SW(SType),
    XOR(RType),
}

impl Instruction {
    const INT_REG_REG_OPCODE: u32 = 0b0110011;
    const INT_REG_IMM_OPCODE: u32 = 0b0010011;
    const LOAD_OPCODE: u32 = 0b0000011;
    const STORE_OPCODE: u32 = 0b0100011;
    const ECALL_EBREAK_OPCODE: u32 = 0b1110011;
    const JAL_OPCODE: u32 = 0b1101111;
    const JALR_OPCODE: u32 = 0b1100111;
    const BRANCH_OPCODE: u32 = 0b1100011;

    pub fn exec(&self, cpu: &mut QDCPU) {
        match self {
            Instruction::Add(ops) => {
                let r = (cpu.get_reg(ops.rs1) as i64) + (cpu.get_reg(ops.rs2) as i64);
                cpu.set_reg(ops.rd, r as u32);
                cpu.pc += 4;
            }
            Instruction::AddI(ops) => {
                let r = (cpu.get_reg(ops.rs) as i64) + (ops.imm as i64);
                cpu.set_reg(ops.rd, r as u32);
                cpu.pc += 4;
            }
            Instruction::BEQ(ops) => {
                if cpu.get_reg(ops.rs1) == cpu.get_reg(ops.rs2) {
                    cpu.pc = (((cpu.pc as i64) + (ops.imm as i64)) as u32) as usize;
                } else {
                    cpu.pc += 4;
                }
            }
            Instruction::BGE(ops) => {
                if (cpu.get_reg(ops.rs1) as i64) >= (cpu.get_reg(ops.rs2) as i64) {
                    cpu.pc = (((cpu.pc as i64) + (ops.imm as i64)) as u32) as usize;
                } else {
                    cpu.pc += 4;
                }
            }
            Instruction::BLT(ops) => {
                if (cpu.get_reg(ops.rs1) as i64) < (cpu.get_reg(ops.rs2) as i64) {
                    cpu.pc = (((cpu.pc as i64) + (ops.imm as i64)) as u32) as usize;
                } else {
                    cpu.pc += 4;
                }
            }
            Instruction::BNE(ops) => {
                if cpu.get_reg(ops.rs1) != cpu.get_reg(ops.rs2) {
                    cpu.pc = (((cpu.pc as i64) + (ops.imm as i64)) as u32) as usize;
                } else {
                    cpu.pc += 4;
                }
            }
            Instruction::DIV(ops) => {
                let divisor = cpu.get_reg(ops.rs2) as i32;
                let dividend = cpu.get_reg(ops.rs1) as i32;

                let quotient = dividend.checked_div(divisor).unwrap_or(-1);
                cpu.set_reg(ops.rd, to2c(quotient));

                cpu.pc += 4;
            }
            Instruction::EBREAK(_) => {
                //
            }
            Instruction::ECALL(_) => {
                // "syscall" in x17
                // params in x10 to x15 (x16 unused)
                if cpu.get_reg(17) == 1 {
                    // print
                    let mut addr = cpu.get_reg(10) as usize;
                    loop {
                        if cpu.mem[addr] == 0 {
                            break;
                        }
                        print!("{}", char::from(cpu.mem[addr]));
                        addr += 1;
                    }
                }
                cpu.pc += 4;
            }
            Instruction::JAL(ops) => {
                cpu.set_reg(ops.rd, (cpu.pc as u32) + 4);
                cpu.pc = (((cpu.pc as i64) + (ops.imm as i64)) as u32) as usize;
            }
            Instruction::JALR(ops) => {
                let addr = (ops.imm + cpu.get_reg(ops.rs)) & (u32::MAX - 1);
                cpu.set_reg(ops.rd, (cpu.pc as u32) + 4);
                cpu.pc = addr as usize;
            }
            Instruction::LB(ops) => {
                let addr = ((cpu.get_reg(ops.rs) as i64) + (ops.imm as i64)) as usize;
                cpu.set_reg(ops.rd, sign_expanded_value(cpu.mem[addr] as u32, 0, 8));
                cpu.pc += 4;
            }
            Instruction::LW(ops) => {
                let addr = ((cpu.get_reg(ops.rs) as i64) + (ops.imm as i64)) as usize;
                let v = (cpu.mem[addr] as u32)
                    | ((cpu.mem[addr + 1] as u32) << 8)
                    | ((cpu.mem[addr + 2] as u32) << 16)
                    | ((cpu.mem[addr + 3] as u32) << 24);
                cpu.set_reg(ops.rd, v);
                cpu.pc += 4;
            }
            Instruction::MUL(ops) => {
                let r = (cpu.get_reg(ops.rs1) as i64) * (cpu.get_reg(ops.rs2) as i64);
                cpu.set_reg(ops.rd, r as u32);
                cpu.pc += 4;
            }
            Instruction::REM(ops) => {
                let modulus = cpu.get_reg(ops.rs2) as i32;
                let dividend = cpu.get_reg(ops.rs1) as i32;

                let remainder = dividend.checked_rem(modulus).unwrap_or(dividend);
                cpu.set_reg(ops.rd, to2c(remainder));

                cpu.pc += 4;
            }
            Instruction::SB(ops) => {
                let addr = ((cpu.get_reg(ops.rs1) as i64) + (ops.imm as i64)) as usize;
                cpu.mem[addr] = (cpu.get_reg(ops.rs2) & 0xFF) as u8;
                cpu.pc += 4;
            }
            Instruction::SltI(ops) => {
                let reg_val = u32::from((cpu.get_reg(ops.rs) as i64) < (ops.imm as i64));
                cpu.set_reg(ops.rd, reg_val);
                cpu.pc += 4;
            }
            Instruction::Sub(ops) => {
                let r = (cpu.get_reg(ops.rs1) as i64) - (cpu.get_reg(ops.rs2) as i64);
                cpu.set_reg(ops.rd, r as u32);
                cpu.pc += 4;
            }
            Instruction::SW(ops) => {
                let addr = ((cpu.get_reg(ops.rs1) as i64) + (ops.imm as i64)) as usize;
                let v = cpu.get_reg(ops.rs2);
                cpu.mem[addr] = (v & 0xFF) as u8;
                cpu.mem[addr + 1] = ((v >> 8) & 0xFF) as u8;
                cpu.mem[addr + 2] = ((v >> 16) & 0xFF) as u8;
                cpu.mem[addr + 3] = ((v >> 24) & 0xFF) as u8;
                cpu.pc += 4;
            }
            Instruction::XOR(ops) => {
                let res = cpu.get_reg(ops.rs1) ^ cpu.get_reg(ops.rs2);
                cpu.set_reg(ops.rd, res);
                cpu.pc += 4;
            }
        }
    }

    pub fn encode(&self) -> u32 {
        match self {
            Instruction::Add(ops) => ops.encode(Instruction::INT_REG_REG_OPCODE),
            Instruction::AddI(ops) => ops.encode(Instruction::INT_REG_IMM_OPCODE),
            Instruction::BEQ(ops) => ops.encode(Instruction::BRANCH_OPCODE),
            Instruction::BGE(ops) => ops.encode(Instruction::BRANCH_OPCODE),
            Instruction::BLT(ops) => ops.encode(Instruction::BRANCH_OPCODE),
            Instruction::BNE(ops) => ops.encode(Instruction::BRANCH_OPCODE),
            Instruction::DIV(ops) => ops.encode(Instruction::INT_REG_REG_OPCODE),
            Instruction::EBREAK(ops) => ops.encode(Instruction::ECALL_EBREAK_OPCODE),
            Instruction::ECALL(ops) => ops.encode(Instruction::ECALL_EBREAK_OPCODE),
            Instruction::JAL(ops) => ops.encode(Instruction::JAL_OPCODE),
            Instruction::JALR(ops) => ops.encode(Instruction::JALR_OPCODE),
            Instruction::LB(ops) => ops.encode(Instruction::LOAD_OPCODE),
            Instruction::LW(ops) => ops.encode(Instruction::LOAD_OPCODE),
            Instruction::MUL(ops) => ops.encode(Instruction::INT_REG_REG_OPCODE),
            Instruction::REM(ops) => ops.encode(Instruction::INT_REG_REG_OPCODE),
            Instruction::SB(ops) => ops.encode(Instruction::STORE_OPCODE),
            Instruction::SltI(ops) => ops.encode(Instruction::INT_REG_IMM_OPCODE),
            Instruction::Sub(ops) => ops.encode(Instruction::INT_REG_REG_OPCODE),
            Instruction::SW(ops) => ops.encode(Instruction::STORE_OPCODE),
            Instruction::XOR(ops) => ops.encode(Instruction::INT_REG_REG_OPCODE),
        }
    }

    pub fn decode(instr: u32) -> Result<Box<Instruction>, DecodingError> {
        Ok(Box::new(match instr & 0b1111111 {
            Instruction::INT_REG_IMM_OPCODE => match bits_value(instr, 12, 15) {
                0 => Instruction::AddI(IType::decode(instr)),
                other => return Err(UnsupportedVariant(Instruction::INT_REG_IMM_OPCODE, other)),
            },
            Instruction::INT_REG_REG_OPCODE => match bits_value(instr, 25, 32) {
                0 => match bits_value(instr, 12, 15) {
                    0 => Instruction::Add(RType::decode(instr)),
                    0b100 => Instruction::XOR(RType::decode(instr)),
                    other => {
                        return Err(UnsupportedSubVariant(
                            Instruction::INT_REG_REG_OPCODE,
                            0,
                            other,
                        ))
                    }
                },
                1 => match bits_value(instr, 12, 15) {
                    0 => Instruction::MUL(RType::decode(instr)),
                    0b100 => Instruction::DIV(RType::decode(instr)),
                    0b110 => Instruction::REM(RType::decode(instr)),
                    other => {
                        return Err(UnsupportedSubVariant(
                            Instruction::INT_REG_REG_OPCODE,
                            1,
                            other,
                        ))
                    }
                },
                0b0100000 => Instruction::Sub(RType::decode(instr)),
                other => return Err(UnsupportedVariant(Instruction::INT_REG_REG_OPCODE, other)),
            },
            Instruction::LOAD_OPCODE => match bits_value(instr, 12, 15) {
                0 => Instruction::LB(IType::decode(instr)),
                0b010 => Instruction::LW(IType::decode(instr)),
                other => return Err(UnsupportedVariant(Instruction::LOAD_OPCODE, other)),
            },
            Instruction::STORE_OPCODE => match bits_value(instr, 12, 15) {
                0 => Instruction::SB(SType::decode(instr)),
                0b010 => Instruction::SW(SType::decode(instr)),
                other => return Err(UnsupportedVariant(Instruction::STORE_OPCODE, other)),
            },
            Instruction::JAL_OPCODE => Instruction::JAL(JType::decode(instr)),
            Instruction::JALR_OPCODE => Instruction::JALR(IType::decode(instr)),
            Instruction::BRANCH_OPCODE => match bits_value(instr, 12, 15) {
                0 => Instruction::BEQ(BType::decode(instr)),
                1 => Instruction::BNE(BType::decode(instr)),
                4 => Instruction::BLT(BType::decode(instr)),
                5 => Instruction::BGE(BType::decode(instr)),
                other => return Err(UnsupportedVariant(Instruction::BRANCH_OPCODE, other)),
            },
            Instruction::ECALL_EBREAK_OPCODE => match bits_value(instr, 20, 32) {
                0 => Instruction::ECALL(IType::decode(instr)),
                1 => Instruction::EBREAK(IType::decode(instr)),
                other => return Err(UnsupportedVariant(Instruction::ECALL_EBREAK_OPCODE, other)),
            },
            other => return Err(UnsupportedOpcode(other)),
        }))
    }
}

pub fn add(rd: RegisterIndex, rs1: RegisterIndex, rs2: RegisterIndex) -> Instruction {
    Instruction::Add(RType {
        rd,
        rs1,
        rs2,
        funct3: 0,
        funct7: 0,
    })
}

pub fn addi(rd: RegisterIndex, rs: RegisterIndex, imm: i32) -> Instruction {
    Instruction::AddI(IType {
        rd,
        rs,
        imm: to2c(imm),
        funct3: 0,
    })
}

pub fn beq(rs1: RegisterIndex, rs2: RegisterIndex, imm: i32) -> Instruction {
    Instruction::BEQ(BType {
        rs1,
        rs2,
        imm: to2c(imm),
        funct3: 0,
    })
}

pub fn bge(rs1: RegisterIndex, rs2: RegisterIndex, imm: i32) -> Instruction {
    Instruction::BGE(BType {
        rs1,
        rs2,
        imm: to2c(imm),
        funct3: 5,
    })
}

pub fn blt(rs1: RegisterIndex, rs2: RegisterIndex, imm: i32) -> Instruction {
    Instruction::BLT(BType {
        rs1,
        rs2,
        imm: to2c(imm),
        funct3: 4,
    })
}

pub fn bne(rs1: RegisterIndex, rs2: RegisterIndex, imm: i32) -> Instruction {
    Instruction::BNE(BType {
        rs1,
        rs2,
        imm: to2c(imm),
        funct3: 1,
    })
}

pub fn div(rd: RegisterIndex, rs1: RegisterIndex, rs2: RegisterIndex) -> Instruction {
    Instruction::DIV(RType {
        rd,
        rs1,
        rs2,
        funct3: 0b100,
        funct7: 1,
    })
}

pub fn ebreak() -> Instruction {
    Instruction::EBREAK(IType {
        rs: 0,
        rd: 0,
        imm: 1,
        funct3: 0,
    })
}

pub fn ecall() -> Instruction {
    Instruction::ECALL(IType {
        rs: 0,
        rd: 0,
        imm: 0,
        funct3: 0,
    })
}

pub fn jal(imm: i32, rd: RegisterIndex) -> Instruction {
    Instruction::JAL(JType { rd, imm: to2c(imm) })
}

pub fn jalr(imm: i32, rs: RegisterIndex, rd: RegisterIndex) -> Instruction {
    Instruction::JALR(IType {
        rd,
        rs,
        imm: to2c(imm),
        funct3: 0,
    })
}

pub fn lb(rd: RegisterIndex, rs: RegisterIndex, imm: i32) -> Instruction {
    Instruction::LB(IType {
        rd,
        rs,
        imm: to2c(imm),
        funct3: 0,
    })
}

pub fn lw(rd: RegisterIndex, rs: RegisterIndex, imm: i32) -> Instruction {
    Instruction::LB(IType {
        rd,
        rs,
        imm: to2c(imm),
        funct3: 0b010,
    })
}

pub fn mul(rd: RegisterIndex, rs1: RegisterIndex, rs2: RegisterIndex) -> Instruction {
    Instruction::MUL(RType {
        rd,
        rs1,
        rs2,
        funct3: 0,
        funct7: 1,
    })
}

pub fn rem(rd: RegisterIndex, rs1: RegisterIndex, rs2: RegisterIndex) -> Instruction {
    Instruction::REM(RType {
        rd,
        rs1,
        rs2,
        funct3: 0b110,
        funct7: 1,
    })
}

pub fn sb(rs1: RegisterIndex, rs2: RegisterIndex, imm: i32) -> Instruction {
    Instruction::SB(SType {
        rs1,
        rs2,
        imm: to2c(imm),
        funct3: 0,
    })
}

pub fn sub(rd: RegisterIndex, rs1: RegisterIndex, rs2: RegisterIndex) -> Instruction {
    Instruction::Sub(RType {
        rd,
        rs1,
        rs2,
        funct3: 0,
        funct7: 0b0100000,
    })
}

pub fn sw(rs1: RegisterIndex, rs2: RegisterIndex, imm: i32) -> Instruction {
    Instruction::SW(SType {
        rs1,
        rs2,
        imm: to2c(imm),
        funct3: 0b010,
    })
}

pub fn nop() -> Instruction {
    addi(0, 0, 0)
}

pub fn xor(rd: RegisterIndex, rs1: RegisterIndex, rs2: RegisterIndex) -> Instruction {
    Instruction::XOR(RType {
        rd,
        rs1,
        rs2,
        funct3: 0b100,
        funct7: 0,
    })
}
