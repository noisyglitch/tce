pub fn bits_value(v: u32, start: u32, end: u32) -> u32 {
    (v >> start) & ((1 << (end - start)) - 1)
}

pub fn sign_expanded_value(v: u32, start: u32, end: u32) -> u32 {
    // todo: refactor to a branchless version
    let sign_bit = v & (1 << (end - 1));
    let prefix = if sign_bit > 0 {
        ((sign_bit >> start) - 1) ^ 0xFFFFFFFF
    } else {
        0
    };
    prefix | bits_value(v, start, end)
}

pub fn to2c(i: i32) -> u32 {
    i as u32
}
