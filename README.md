# Toy CPU Emulator

Toy project that allows me to:
- learn Rust
- explore the RISC-V ISA spec 

Moreover, the aim is to avoid using external crates (there is a learning opportunity in reinventing the wheel).

Made of 2 binaries:
- a CPU emulator (`tce.rs`)
- an assembler (`as.rs`)

It is not a complete RISC-V emulator by any means. Only a small subset of instructions has been implemented.

An example asm source file may be found in the `asm` directory.
Quickest way to test it:

compile: `cargo run --bin as -- -i asm/mower.S -o /tmp/mower.bin`

run: `cargo run --bin tce -- -i /tmp/mower.bin`

The code comes with no guarantee whatsoever, use at your own risk !
